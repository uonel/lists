package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/emersion/go-message/mail"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/komkom/toml"

	"github.com/duo-labs/webauthn/webauthn"
)

var store = newCookieStore()
var authn = newWebauthn()
var logincodes = make(map[string]struct {
	user   string
	issued time.Time
})

func newCookieStore() *sessions.CookieStore {
	auth, encr := getSecrets()
	st := sessions.NewCookieStore(auth, encr)
	st.Options.Secure = true
	st.Options.SameSite = http.SameSiteNoneMode
	//st.Options.MaxAge = 60 * 24 * 7 * 365
	return st
}

func newWebauthn() *webauthn.WebAuthn {
	w, err := webauthn.New(&webauthn.Config{
		RPDisplayName: "The Lists App",
		RPID:          host,
		RPOrigin:      "https://" + host,
	})
	if err != nil {
		panic(err)
	}
	return w
}

func ReadUser(username string, includeLists bool) (*User, error) {
	username = strings.ToLower(username)
	dir, err := ioutil.ReadDir(username)
	if err != nil {
		return nil, err
	}
	files := []*List{}
	if includeLists {
		for _, file := range dir {
			if file.Name()[0] != []byte(".")[0] && !file.IsDir() {
				list, err := readList(username, file.Name())
				if err != nil {
					Log.Println(err)
				}
				files = append(files, list)
			}
		}
	}
	return &User{
		Username: username,
		Lists:    files,
	}, nil
}

func newLoginCode(username string) string {
	chars := []rune("CDEFHJKMNPRTVWXY2345689")
	var b [6]byte
	rand.Reader.Read(b[:])
	code := make([]rune, 6)
	for i, v := range b {
		code[i] = chars[int(v)%len(chars)]
	}
	logincodes[string(code)] = struct {
		user   string
		issued time.Time
	}{
		user:   username,
		issued: time.Now(),
	}
	return string(code)
}

func checkLoginCode(code string) (u string, ok bool) {
	code = strings.ToUpper(code)
	for k, v := range logincodes {
		if time.Since(v.issued).Hours() > 1 {
			delete(logincodes, k)
			return "", false
		} else if k == code {
			delete(logincodes, k)
			return v.user, true
		}
	}
	return "", false
}

func getSecrets() ([]byte, []byte) {
	key, err := ioutil.ReadFile("sessionkey")
	if err != nil {
		if os.IsNotExist(err) {
			var auth, encr [32]byte
			rand.Read(auth[:])
			rand.Read(encr[:])
			err := ioutil.WriteFile("sessionkey", append(auth[:], encr[:]...), 0600)
			if err != nil {
				Log.Println(err)
				os.Exit(1)
			}
			return auth[:], encr[:]
		}
		Log.Println(err)
		os.Exit(1)
	}
	if len(key) != 64 {
		err := os.Remove("sessionkey")
		if err != nil {
			Log.Println(err)
			os.Exit(1)
		}
		return getSecrets()
	}
	return key[:32], key[32:]
}

func SignedIn(r *http.Request) (string, bool) {
	session, _ := store.Get(r, "session")
	username, ok := session.Values["username"]
	if !ok {
		return "", false
	}
	switch username.(type) {
	case string:
		if u := username.(string); u != "" {
			return u, true
		}
		return "", false
	}
	return "", false
}

func logIn(username string, w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session")
	session.Values["username"] = strings.ToLower(username)
	session.Save(r, w)
}

func logOut(username string, w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session")
	session.Values["username"] = ""
	session.Save(r, w)
}

func CheckLoggedIn(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if s := r.URL.Query().Get("session"); s != "" {
			cookie := sessions.NewCookie("session", s, store.Options)
			cookie.MaxAge = store.Options.MaxAge
			http.SetCookie(w, cookie)
			query := r.URL.Query()
			query.Del("session")
			http.Redirect(w, r, r.URL.Path+"?"+query.Encode(), http.StatusFound)
			return
		}
		if username, ok := SignedIn(r); ok {
			logIn(username, w, r) // reset cookie
			fn(w, r, username)
		} else {
			if r.Header.Get("Upgrade") == "websocket" {
				websocketLogIn(w, r)
			} else {
				http.Redirect(w, r, "/-/login", http.StatusFound)
			}
		}
	}
}

func generateToken(username string) (string, error) {
	session := sessions.NewSession(store, "session")
	session.Values["username"] = username
	// https://github.com/gorilla/sessions/blob/v1.2.1/store.go#L102
	return securecookie.EncodeMulti(session.Name(), session.Values, store.Codecs...)
}

func readAlias(username string) string {
	aliasf, _ := ioutil.ReadFile("aliases.toml")
	aliases := make(map[string]string)
	json.NewDecoder(toml.New(bytes.NewBuffer(aliasf))).Decode(&aliases)

	if alias, ok := aliases[username]; ok {
		return alias
	}
	return ""
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		r.ParseForm()
		if r.Form.Has("session") {
			cookie := sessions.NewCookie("session", r.FormValue("session"), store.Options)
			http.SetCookie(w, cookie)
			addMessage(w, r, "Login successful")
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		tdata := TemplateData{
			Messages: getMessages(w, r),
		}
		templates.ExecuteTemplate(w, TemplLogin, tdata)
		return
	case "POST":
		email := strings.ToLower(r.FormValue("username"))

		if alias := readAlias(email); alias != "" {
			email = alias
		}

		p, err := mail.ParseAddress(email)
		if err != nil {
			if user, ok := checkLoginCode(email); ok {
				logIn(user, w, r)
				addMessage(w, r, "Login successful")
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
			addMessage(w, r, "Invalid email address")
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		email = p.Address

		// check if user exists
		if s, err := os.Stat(email); err != nil || !s.IsDir() {
			addMessage(w, r, "User "+email+" does not exist")
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		encoded, err := generateToken(email)
		if err != nil {
			Log.Println(err)
			addMessage(w, r, "Internal error")
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		values := url.Values{"session": {encoded}}
		link := "https://" + r.Host + "/login" + "?" + values.Encode()
		code := newLoginCode(email)
		go send_link(email, link, code)
		addMessage(w, r, "Login link sent to "+email)
		http.Redirect(w, r, "/login", http.StatusFound)
	default:
		addMessage(w, r, "Fehler!")
		http.Redirect(w, r, "/", http.StatusFound)
	}

}

func LogoutHandler(w http.ResponseWriter, r *http.Request, username string) {
	logOut(username, w, r)
	http.Redirect(w, r, "/login", http.StatusFound)
}

func NewAliasHandler(w http.ResponseWriter, r *http.Request, username string) {
	alias := r.FormValue("alias")
	if alias == "" {
		addMessage(w, r, "Alias darf nicht leer sein")
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	site := r.FormValue("url")
	if site == "" {
		addMessage(w, r, "URL darf nicht leer sein")
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	urlf, _ := ioutil.ReadFile("urls.toml")
	urlm := make(map[string]string)
	json.NewDecoder(toml.New(bytes.NewBuffer(urlf))).Decode(&urlm)
	if _, ok := urlm[alias]; ok {
		addMessage(w, r, "Alias existiert bereits")
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	uri, err := url.Parse(site)
	if err != nil {
		addMessage(w, r, "Keine valide URL")
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	uri.Scheme = ""
	site = uri.String()[2:]

	// append new alias to file
	f, err := os.OpenFile("urls.toml", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		Log.Println(err)
		addMessage(w, r, "Server Fehler: "+err.Error())
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	defer f.Close()
	if _, err := f.WriteString(alias + " = \"" + site + "\"\n"); err != nil {
		Log.Println(err)
		addMessage(w, r, "Server Fehler: "+err.Error())
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	addMessage(w, r, "Alias erfolgreich hinzugefügt")
	http.Redirect(w, r, "/lists", http.StatusFound)
}

func readCreds(username string) (creds map[string]webauthn.Credential) {
	creds = make(map[string]webauthn.Credential)
	if strings.Contains(username, "/") {
		return
	}
	file, err := os.Open(username + "/.webauthn_credentials.json")
	if err != nil {
		return
	}
	defer file.Close()

	json.NewDecoder(file).Decode(&creds)
	return
}

func deleteCred(username, cred string) {
	creds := readCreds(username)
	delete(creds, cred)
	saveCreds(username, creds)
}

func saveCreds(username string, creds map[string]webauthn.Credential) error {
	if strings.Contains(username, "/") {
		return errors.New("invalid username")
	}
	file, err := os.Create(username + "/.webauthn_credentials.json")
	if err != nil {
		return errors.New("Error writing file: " + err.Error())
	}
	defer file.Close()
	err = json.NewEncoder(file).Encode(creds)
	if err != nil {
		return errors.New("Error encoding creds: " + err.Error())
	}
	return nil
}

func AuthnRegistration(w http.ResponseWriter, r *http.Request, username string) {
	switch r.Method {
	case "GET":
		user, err := ReadUser(username, false)
		if err != nil {
			addMessage(w, r, "Fehler beim key hinzufügen")
			return
		}
		credname := r.URL.Query().Get("name")

		options, sessionData, err := authn.BeginRegistration(user)

		session, _ := store.Get(r, "webauthn")
		session.Values["webauthn"] = sessionData
		session.Values["name"] = credname
		err = session.Save(r, w)
		if err != nil {
			JSONResponse(w, "Error saving cookie: "+err.Error(), http.StatusInternalServerError)
			return
		}

		JSONResponse(w, options, http.StatusOK)
	case "POST":
		// Read User
		user, err := ReadUser(username, false)
		if err != nil {
			JSONResponse(w, "Error reading user", http.StatusBadRequest)
			return
		}

		// read SessionData
		session, err := store.Get(r, "webauthn")
		if err != nil {
			JSONResponse(w, "Error reading cookie", http.StatusBadRequest)
			return
		}
		intf, ok := session.Values["webauthn"]
		if !ok {
			JSONResponse(w, "Error reading sessionData from cookie", http.StatusBadRequest)
			return
		}
		sessionData, ok := intf.(*webauthn.SessionData)
		if !ok {
			JSONResponse(w, "Error typecasting into SessionData", http.StatusBadRequest)
			return
		}

		credential, err := authn.FinishRegistration(user, *sessionData, r)
		if err != nil {
			JSONResponse(w, err, http.StatusInternalServerError)
			return
		}

		var credname string
		cn, ok := session.Values["name"]
		if ok {
			credname, _ = cn.(string)
		}

		creds := readCreds(username)
		creds[credname] = *credential
		err = saveCreds(username, creds)
		if err != nil {
			JSONResponse(w, "Error saving credential: "+err.Error(), http.StatusInternalServerError)
			return
		}
		JSONResponse(w, "Registration was Successfull", http.StatusOK)
	}
}

func AuthnLogin(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		username := r.URL.Query().Get("user")
		if a := readAlias(username); a != "" {
			username = a
		}
		user, err := ReadUser(username, false)
		if err != nil {
			fmt.Println(err, err.Error())
			JSONResponse(w, err, http.StatusNotFound)
			return
		}

		options, sessionData, err := authn.BeginLogin(user)
		if err != nil {
			JSONResponse(w, err, http.StatusInternalServerError)
		}

		session, _ := store.Get(r, "webauthn")
		session.Values["webauthn"] = sessionData
		session.Values["username"] = username
		err = session.Save(r, w)
		if err != nil {
			JSONResponse(w, err, http.StatusInternalServerError)
		}

		JSONResponse(w, options, http.StatusOK)
	case "POST":
		session, _ := store.Get(r, "webauthn")
		u, ok := session.Values["username"]
		if !ok {
			JSONResponse(w, "Missing username in webauthn cookie", http.StatusInternalServerError)
			return
		}
		username, _ := u.(string)
		user, err := ReadUser(username, false)
		if err != nil {
			JSONResponse(w, "Couldn't find user", http.StatusInternalServerError)
			return
		}
		s, ok := session.Values["webauthn"]
		if !ok {
			JSONResponse(w, "WebAuthn cookie missing", http.StatusInternalServerError)
			return
		}
		sessionData, ok := s.(*webauthn.SessionData)
		if !ok {
			JSONResponse(w, "Error typecasting", http.StatusInternalServerError)
			return
		}
		_, err = authn.FinishLogin(user, *sessionData, r)
		if err != nil {
			JSONResponse(w, err, http.StatusUnauthorized)
			return
		}
		logIn(username, w, r)

	}
}
