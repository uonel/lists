package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	listb "github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
)

type lists struct {
	lists         []item
	list          listb.Model
	heightchanged bool
}

type listsItemDelegate struct{}

func (d listsItemDelegate) Height() int                                { return 1 }
func (d listsItemDelegate) Spacing() int                               { return 0 }
func (d listsItemDelegate) Update(msg tea.Msg, m *listb.Model) tea.Cmd { return nil }
func (d listsItemDelegate) Render(w io.Writer, m listb.Model, index int, listItem listb.Item) {
	i, ok := listItem.(item)
	if !ok {
		return
	}

	str := fmt.Sprintf("%s", i)

	fn := itemStyle.Render
	if index == m.Index() {
		fn = func(s string) string {
			return selectedItemStyle.Width(m.Width()).Render(s)
		}
	}

	fmt.Fprintf(w, fn(str))
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

type item string

func (i item) Title() string {
	return string(i)
}

func (i item) Description() string {
	return ""
}

func (i item) FilterValue() string {
	return string(i)
}

func (l lists) Init() tea.Cmd {
	return func() tea.Msg {
		return getLists()
	}
}

func (l lists) View() string {
	return "\n" + l.list.View()
}

func (l lists) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return l, tea.Quit
		case "enter":
			i, ok := l.list.SelectedItem().(item)
			if ok {
				return getList(string(i)), nil
			}
			return l, tea.Quit
		}
	case tea.WindowSizeMsg:
		l.list.SetWidth(msg.Width)
		if l.heightchanged || msg.Height < l.list.Height() {
			l.list.SetHeight(msg.Height)
			l.heightchanged = true
		}
	}

	var cmd tea.Cmd
	l.list, cmd = l.list.Update(msg)
	return l, cmd
}

const defaultWidth = 60
const defaultHeight = 20

func getLists() lists {
	// request hostname/lists with cookie and accept json
	req, err := http.NewRequest("GET", "https://"+hostname+"/lists", nil)
	if err != nil {
		panic(err)
	}
	req.AddCookie(&http.Cookie{Name: "session", Value: cookie})
	req.Header.Set("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	var data struct {
		User  string
		Lists []string
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		r, _ := io.ReadAll(resp.Body)
		fmt.Fprintf(os.Stderr, "error: %s\n", string(r))
		fmt.Println("Error reading resp:", err)
		os.Exit(1)
	}
	var m lists
	items := []listb.Item{}
	for _, name := range data.Lists {
		items = append(items, item(name))
	}
	m.list = listb.New(items, listsItemDelegate{}, defaultWidth, defaultHeight+1)
	m.list.Title = data.User
	m.list.Styles.PaginationStyle = paginationStyle
	m.list.Styles.HelpStyle = helpStyle

	return m
}
