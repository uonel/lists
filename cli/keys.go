package main

import (
	"github.com/charmbracelet/bubbles/key"
)

type ListKeyMap struct {
	// taken from https://github.com/charmbracelet/bubbles/blob/v0.10.3/list/keys.go#L7
	CursorUp   key.Binding
	CursorDown key.Binding
	NextPage   key.Binding
	PrevPage   key.Binding
	GoToStart  key.Binding
	GoToEnd    key.Binding

	Sync   key.Binding
	Add    key.Binding
	Select key.Binding

	ToggleHelp key.Binding
	Menu       key.Binding
	Quit       key.Binding
	ForceQuit  key.Binding // must contain a modifier key
}

func (k ListKeyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Sync, k.Add, k.Select, k.Menu, k.ToggleHelp}
}

func (k ListKeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.CursorUp, k.CursorDown, k.NextPage, k.PrevPage, k.GoToStart, k.GoToEnd},
		{k.Sync, k.Add, k.Select, k.Menu},
		{k.ToggleHelp, k.Quit, k.ForceQuit},
	}
}

func newKeyMap() *ListKeyMap {
	return &ListKeyMap{
		// Browsing.
		CursorUp: key.NewBinding(
			key.WithKeys("up", "k"),
			key.WithHelp("↑/k", "up"),
		),
		CursorDown: key.NewBinding(
			key.WithKeys("down", "j"),
			key.WithHelp("↓/j", "down"),
		),
		PrevPage: key.NewBinding(
			key.WithKeys("left", "h", "pgup", "b", "u"),
			key.WithHelp("←/h/pgup", "prev page"),
		),
		NextPage: key.NewBinding(
			key.WithKeys("right", "l", "pgdown", "f", "d"),
			key.WithHelp("→/l/pgdn", "next page"),
		),
		GoToStart: key.NewBinding(
			key.WithKeys("home", "g"),
			key.WithHelp("g/home", "go to start"),
		),
		GoToEnd: key.NewBinding(
			key.WithKeys("end", "G"),
			key.WithHelp("G/end", "go to end"),
		),

		Sync: key.NewBinding(
			key.WithKeys("s", "u"),
			key.WithHelp("s/u", "sync"),
		),
		Add: key.NewBinding(
			key.WithKeys("a", "+", "i"),
			key.WithHelp("a/i/+", "add"),
		),
		Select: key.NewBinding(
			key.WithKeys("enter", " "),
			key.WithHelp("↵/space", "select"),
		),

		ToggleHelp: key.NewBinding(
			key.WithKeys("?", "H"),
			key.WithHelp("?", "toggle help"),
		),
		Menu: key.NewBinding(
			key.WithKeys("m"),
			key.WithHelp("m", "menu"),
		),
		Quit: key.NewBinding(
			key.WithKeys("q"),
			key.WithHelp("q", "quit"),
		),
		ForceQuit: key.NewBinding(
			key.WithKeys("ctrl+c"),
		),
	}
}
