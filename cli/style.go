package main

import (
	listb "github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/lipgloss"
)

var docStyle = lipgloss.NewStyle().Margin(1, 2)
var (
	titleStyle        = lipgloss.NewStyle().MarginLeft(2)
	itemStyle         = lipgloss.NewStyle().PaddingLeft(4)
	selectedItemStyle = lipgloss.NewStyle().
				PaddingLeft(4).
				Foreground(lipgloss.Color("#ccb8f2")).
				Background(lipgloss.AdaptiveColor{Light: "#f1f1f1", Dark: "#373737"})
	paginationStyle = listb.DefaultStyles().PaginationStyle.PaddingLeft(4)
	helpStyle       = listb.DefaultStyles().HelpStyle.PaddingLeft(4).PaddingBottom(1)
	quitTextStyle   = lipgloss.NewStyle().Margin(1, 0, 2, 4)
	focusedStyle    = lipgloss.NewStyle().Foreground(lipgloss.Color("205")).PaddingLeft(4)
)
