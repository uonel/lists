package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

var (
	hostname = ""
	cookie   = ""
	client   = http.Client{}
)

func initalModel() lists {
	return getLists()
}

type errMsg struct {
	err error
}

func (e errMsg) Error() string {
	return e.err.Error()
}

type config struct {
	cookie string
}
type visitLists bool
type visitList string

func readConfig() (host string, cookie string) {
	// read ~/.config/lists.config
	hd, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("Error finding home dir:", err)
	}
	u, err := os.ReadFile(hd + "/.config/lists.config")
	if err != nil {
		fmt.Println("Error reading config:", err)
		return "", ""
	}

	// parse url
	s := strings.Split(string(u), "\n")[0]
	p, err := url.Parse(s)
	if err != nil {
		fmt.Println("Error Reading config:", err)
		return "", ""
	}
	return p.Host, p.Query().Get("session")
}

func main() {
	hostname, cookie = readConfig()
	if hostname == "" || cookie == "" {
		fmt.Println("Please set your hostname and cookie in ~/.config/lists.config")
		os.Exit(1)
	}
	listname := strings.Join(os.Args[1:], " ")
	var list tea.Model
	if listname == "" {
		list = getLists()
	} else {
		list = getList(listname)
	}
	p := tea.NewProgram(list)
	if err := p.Start(); err != nil {
		fmt.Printf("There's been an error: %v", err)
		os.Exit(1)
	}
}
