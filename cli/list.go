package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	listb "github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type list struct {
	selected map[int]bool
	list     listb.Model
	input    textinput.Model
	keys     *ListKeyMap
	help     help.Model
}

type listItem struct {
	name string
	list *list
}

func (i listItem) FilterValue() string {
	return i.name
}

func (i listItem) Title() string {
	return i.name
}

func (i listItem) String() string {
	return i.name
}

func (i listItem) Update(msg tea.Msg, m *listb.Model) tea.Cmd {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, i.list.keys.Select):
			index := m.Index()
			if v, ok := i.list.selected[index]; ok {
				i.list.selected[index] = !v
			} else {
				i.list.selected[index] = true
			}
			return m.SetItem(index, i)
		}
	}
	return nil
}

type keyEnableCmd struct{}

func msgCmd(msg tea.Msg) func() tea.Msg {
	return func() tea.Msg {
		return msg
	}
}

type listItemDelegate struct{}

func (d listItemDelegate) Height() int  { return 1 }
func (d listItemDelegate) Spacing() int { return 0 }
func (d listItemDelegate) Update(msg tea.Msg, m *listb.Model) tea.Cmd {
	if itm, ok := m.SelectedItem().(listItem); ok {
		return itm.Update(msg, m)
	} else if itm, ok := m.SelectedItem().(inputItem); ok {
		ti, cmd := itm.textinput.Update(msg)
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.Type {
			case tea.KeyEnter:
				return tea.Batch(m.StartSpinner(), itm.list.posti(m.Index()))
			case tea.KeyEscape:
				itm.textinput.Blur()
				cmd = m.SetItem(m.Index(), inputItem{
					textinput: itm.textinput,
					list:      itm.list})
				return tea.Batch(cmd, msgCmd(keyEnableCmd{}))
			}
		}
		return tea.Batch(cmd, m.SetItem(m.Index(), inputItem(inputItem{textinput: &ti,
			list: itm.list})))
	}
	return nil
}

func (d listItemDelegate) Render(w io.Writer, m listb.Model, index int, lItem listb.Item) {
	if i, ok := lItem.(listItem); ok {
		str := fmt.Sprintf("%s", i)

		style := itemStyle.Copy().Width(m.Width())
		if index == m.Index() {
			style = selectedItemStyle.Copy().Width(m.Width())
		}
		if i.list.selected[index] == true {
			style = style.Strikethrough(true)
		}

		fmt.Fprintf(w, style.Render(str))
	} else if i, ok := lItem.(inputItem); ok {
		style := itemStyle.Copy()
		ti := *i.textinput
		if index == m.Index() {
			style = selectedItemStyle.Copy().Width(m.Width())
			ti.PlaceholderStyle = ti.PlaceholderStyle.Copy().
				Background(selectedItemStyle.GetBackground())
		}
		fmt.Fprintf(w, style.Render(ti.View()))
	}
}

type inputItem struct {
	textinput *textinput.Model
	list      *list
}

func (i inputItem) FilterValue() string {
	return i.textinput.Value()
}

func getList(name string, index ...int) tea.Model {
	// request hostname/name
	req, err := http.NewRequest("GET", "https://"+hostname+"/"+name, nil)
	if err != nil {
		fmt.Println("Error preparing request", err)
		os.Exit(1)
	}
	req.AddCookie(&http.Cookie{Name: "session", Value: cookie})
	req.Header.Set("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making List", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	var data struct {
		Title string   `json:"title"`
		Items []string `json:"items"`
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		fmt.Println("Error reading resp:", err)
		os.Exit(1)
	}
	var l list
	// generate textinput
	ti := textinput.New()
	ti.Placeholder = "Anderes..."
	ti.Width = defaultWidth - 5
	ti.Prompt = ""

	// make items
	var items []listb.Item
	for _, item := range data.Items {
		items = append(items, listItem{name: item, list: &l})
	}

	// add input as last item
	items = append(items, inputItem{textinput: &l.input, list: &l})

	// make list bubble
	l.list = listb.New(items, listItemDelegate{}, defaultWidth, defaultHeight)
	l.list.Title = data.Title
	l.keys = newKeyMap()
	l.list.KeyMap = listb.KeyMap{
		CursorUp:   l.keys.CursorUp,
		CursorDown: l.keys.CursorDown,
		NextPage:   l.keys.NextPage,
		PrevPage:   l.keys.PrevPage,
		GoToStart:  l.keys.GoToStart,
		GoToEnd:    l.keys.GoToEnd,
	}
	l.list.SetFilteringEnabled(false)
	l.list.DisableQuitKeybindings()
	l.list.SetShowHelp(false)
	l.list.Styles.PaginationStyle = paginationStyle
	if len(index) > 0 {
		l.list.Select(index[0])
	}
	l.input = ti
	l.help = help.New()
	l.help.Width = defaultWidth - 5
	l.selected = make(map[int]bool)

	return l
}

type update struct {
	index int
}

func (l list) posti(i int) tea.Cmd {
	return func() tea.Msg {
		l.post()
		return update{index: i}
	}
}

func (l list) post() tea.Msg {
	var data struct {
		Add    []string `json:"add"`
		Remove []string `json:"remove"`
		Wait   bool     `json:"wait"`
	}
	litems := l.list.Items()
	initem, ok := litems[len(litems)-1].(inputItem)
	addstring := ""
	if ok {
		addstring = initem.textinput.Value()
	}
	data.Add = strings.Split(addstring, ",")
	for i, item := range l.list.Items() {
		if l.selected[i] {
			data.Remove = append(data.Remove, item.(listItem).name)
		}
	}
	// post data as json to https://hostname/update/listname
	jb, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	req, err := http.NewRequest("POST", "https://"+hostname+"/update/"+l.list.Title,
		bytes.NewBuffer([]byte(jb)))
	if err != nil {
		panic(err)
	}
	req.AddCookie(&http.Cookie{Name: "session", Value: cookie})
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.Status != "200 OK" {
		panic(errMsg{err: fmt.Errorf("%s", resp.Status)})
	}
	time.Sleep(200 * time.Millisecond)
	return update{}
}

func (m list) Init() tea.Cmd {
	return nil
}

func isInput(msg tea.Msg) bool {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyRunes:
			return true
		}
	}
	return false
}

func (l *list) KeysSetEnabled(enabled bool) {
	l.list.KeyMap.CursorUp.SetEnabled(enabled)
	l.list.KeyMap.CursorDown.SetEnabled(enabled)
	l.list.KeyMap.NextPage.SetEnabled(enabled)
	l.list.KeyMap.PrevPage.SetEnabled(enabled)
	l.list.KeyMap.GoToEnd.SetEnabled(enabled)
	l.list.KeyMap.GoToStart.SetEnabled(enabled)

	l.keys.Add.SetEnabled(enabled)
	l.keys.Select.SetEnabled(enabled)
	l.keys.Sync.SetEnabled(enabled)

	l.keys.ToggleHelp.SetEnabled(enabled)
	l.keys.Menu.SetEnabled(enabled)
	l.keys.Quit.SetEnabled(enabled)
}

type Quit struct{}

func (q Quit) View() string                                { return "" }
func (q Quit) Update(msg tea.Msg) (m tea.Model, c tea.Cmd) { return q, tea.Quit }
func (q Quit) Init() tea.Cmd                               { return tea.Quit }

func (l *list) SetHeight() {
	helpheight := lipgloss.Height(l.help.View(l.keys))
	l.list.SetHeight(defaultHeight - helpheight)
}

func (m list) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmds []tea.Cmd
	var cmd tea.Cmd
	itms := m.list.Items()
	input := itms[len(itms)-1].(inputItem)
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.keys.Quit):
			return Quit{}, tea.Quit
		case key.Matches(msg, m.keys.Add):
			m.input.Focus()
			input.textinput.Focus()
			m.list.SetItem(len(itms)-1, input)
			m.KeysSetEnabled(false)
			m.list.Select(len(m.list.Items()) - 1)
			return m, nil
		// case key.Matches(msg, m.keys.Select):
		// 	if m.list.SelectedItem() == m.list.Items()[len(m.list.Items())-1] {
		// 		cmd = m.post
		// 	}
		case key.Matches(msg, m.keys.Sync):
			cmds = append(cmds, m.posti(m.list.Index()))
		case key.Matches(msg, m.keys.ToggleHelp):
			m.help.ShowAll = !m.help.ShowAll
			m.SetHeight()
		case key.Matches(msg, m.keys.Menu):
			return getLists(), nil
		}
	case tea.WindowSizeMsg:
		m.list.SetWidth(msg.Width)
		m.help.Width = msg.Width - 5
	case update:
		return getList(m.list.Title, msg.index), nil
	case keyEnableCmd:
		m.KeysSetEnabled(true)
	}
	m.list, cmd = m.list.Update(msg)
	return m, tea.Batch(append(cmds, cmd)...)
}

func (m list) View() string {
	return "\n" + m.list.View() +
		"\n" + helpStyle.Render(m.help.View(m.keys))
}
