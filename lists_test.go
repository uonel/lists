package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_list(t *testing.T) {
	fmt.Println("## Testing newList()")
	l, e := newList("username", "listname")
	assert.Nil(t, e)

	fmt.Println("## Testing add()")
	e = add("username", l, []string{"a", "b", "c"}, nil)
	if e != nil {
		t.Errorf("Error adding to list: %s", e)
	}
	e = add("username", l, []string{"d", "e", "f"}, nil)
	if e != nil {
		t.Errorf("Error adding to list: %s", e)
	}
	list, e := readList("username", l)
	if e != nil {
		t.Errorf("Error reading list: %s", e)
	}
	assert.Equal(t, []string{"a", "b", "c", "d", "e", "f"}, tostrings(list.Items))

	fmt.Println("## Testing remove()")
	e = remove("username", l, []string{"a", "b", "c"})
	if e != nil {
		t.Errorf("Error removing from list: %s", e)
	}
	list, e = readList("username", l)
	if e != nil {
		t.Errorf("Error reading list: %s", e)
	}
	assert.Equal(t, []string{"d", "e", "f"}, tostrings(list.Items))

	fmt.Println("## Testin move()")
	e = move("username", l, "f", "d") // move f after d
	if e != nil {
		t.Errorf("Error moving items: %s", e)
	}

	list, e = readList("username", l)
	if e != nil {
		t.Errorf("Error reading from list: %s", e)
	}
	fmt.Println(tostrings(list.Items))
	assert.Equal(t, []string{"d", "f", "e"}, tostrings(list.Items))
}
