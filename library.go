package main

/*
import (
	"crypto/sha256"
	"encoding/base64"
	"regexp"
	"strings"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	// regex, that removes all non-alphanumeric characters
	removeNonAlphaNum = regexp.MustCompile(`[^a-zA-Z üäöÜÄÖßẞ]+`)
	// regex, that removes text in parentheses
	removeParens = regexp.MustCompile(`\([^)]*\)`)
	// regex, that removes text after a colon or a hyphen
	removeSeperator = regexp.MustCompile(`[:-].+`)
)

type CacheItem struct {
	ID        uint
	ItemHash  string `gorm:"unique"`
	UserHash  string
	Count     int `gorm:"default:1"`
	UpdatedAt time.Time
}

type LibraryItem struct {
	Item string
}

func cleanItem(item string) string {
	// remove parts after a separator
	item = removeSeperator.ReplaceAllString(item, "")
	// remove parts in parentheses
	item = removeParens.ReplaceAllString(item, "")
	// remove all non-alphanumeric characters
	item = removeNonAlphaNum.ReplaceAllString(item, "")
	// remove leading and trailing whitespace
	item = strings.Trim(item, " \n\t\r")
	return strings.ToLower(item)
}

func initdb() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("library.db"), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&CacheItem{})
	return db
}

var db = initdb()

func trackItem(item string, username string) {
	// clean item
	item = cleanItem(item)
	Log.Println("Tracking item:", item)
	// hash item and username and base64 encode them
	itemHashRaw := sha256.Sum256([]byte(item))
	userHashRaw := sha256.Sum256([]byte(username))
	itemHash := base64.StdEncoding.EncodeToString(itemHashRaw[:])
	userHash := base64.StdEncoding.EncodeToString(userHashRaw[:])
	// check if item is already in the database
	var cacheItem CacheItem
	rslt := db.First(&cacheItem, "item_hash = ?", itemHash)
	// if not, create a new entry
	Log.Println(rslt.Error, rslt.Error == gorm.ErrRecordNotFound)
	if rslt.Error == gorm.ErrRecordNotFound {
		Log.Println("Creating new cache item")
		db.Create(&CacheItem{
			ItemHash: itemHash,
			UserHash: userHash,
			Count:    1,
		})
	} else if rslt.Error != nil {
		Log.Println("Error:", rslt.Error)
		Log.Println("Error while checking if item is already in the database:", rslt.Error)
	} else {
		// if it is, increment the count and set userhash
		cacheItem.Count++
		if cacheItem.Count > 2 && cacheItem.UserHash != userHash {
			Log.Println("New Library Item")
			go send_mail("internal", "New Library Item "+item)
			db.Delete(&cacheItem)
		} else {
			cacheItem.UserHash = userHash
			db.Save(&cacheItem)
		}
	}
}*/
