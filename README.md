# The Lists App

<img src="icon.svg" alt="A shopping list in a shopping basket, which reads 'The Lists App'" width=300 style="float:right;">

## Features

- Magic link login
- Webauthn login
- Share lists with any email address
- sort items via drag and drop
- automatic item sorting
- Real time synchronisation
- Some offline functionality
- Attach images to items
- Share list with limited permissions
- Email notifications on share
- Lists stored as text files in filesystem
- Shares are made with hardlinks

## Usage

1. clone this repository
2. to build the binary run `./build.sh <host>`
3. create a `mail.cred` where the first line will be the email we will send emails from, the second line the password and the third line the email server
4. create a directory named your email adress
5. run the `lists` binary
