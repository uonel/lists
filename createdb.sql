CREATE TABLE neighbors(a TEXT, b TEXT, count INT DEFAULT 1);
CREATE UNIQUE INDEX neighbors_index on neighbors(a, b);
CREATE TABLE prelib(itemhash TEXT, lastuser TEXT, lasttime INT, count INT DEFAULT 1);
