var online = false;

function addItem(name, cleanedname) {
  // check if item is already in list
  if (document.getElementById(cleanedname)) {
    return null;
  }
  var elem = document.createElement("div");
  elem.className = "item";
  elem.addEventListener("dragstart", dragstarthandler);
  elem.innerHTML = `<input
            type="checkbox"
            name="${cleanedname}"
            value="${name}"
            id="${cleanedname}"
            onclick="syncCrossed(this)"
          />
          <label
            for="${cleanedname}"
            class="${cleanedname}label ${cleanedname}"
            draggable="true"
            id="${cleanedname}label"
            ondrag="onDrag(event)"
            ondragend="onendDrag(event)"
            >${name}</label
          >`;

  var last =
    document.getElementById("last").parentElement.previousElementSibling;
  console.log(last);
  last.previousElementSibling.before(elem);
  let space = elem.previousSibling.cloneNode(false);
  space.style.margin = -12;
  elem.before(space);
  return elem;
}

var listname = window.location.pathname.substring(1);

function sendqueue() {
  var hasdata = false;
  var form = document.getElementById("list");
  var addqueue = form.getElementsByClassName("addqueue");
  // join addqueus values with a comma
  var adddata = "";
  var al = addqueue.length;
  for (var i = 0; i < al; i++) {
    // get input for label
    hasdata = true;
    adddata += addqueue[i].previousSibling.value.trim() + ",";
  }
  var addqueue = form.getElementsByClassName("addqueue");
  console.log(addqueue);
  console.log("removing addque");
  al = addqueue.length;
  for (var i = 0; i < al; i++) {
    console.log("removing addqueue", i, addqueue.length);
    console.log(addqueue[0].className);
    addqueue[0].className = addqueue[0].className.replace("addqueue", "");
  }
  adddata = adddata.substring(0, adddata.length - 1);
  console.log(adddata);
  var rmqueue = form.getElementsByClassName("rmqueue");
  var formData = new FormData();
  formData.append("add", adddata);
  // the rm queue has the value as key and true as value
  var al = rmqueue.length;
  for (var i = 0; i < al; i++) {
    var input = document.getElementById(rmqueue[i].innerHTML);
    if (input) {
      hasdata = true;
      formData.append(input.value, input.value);
    }
  }
  for (var i; rmqueue.length > 0; i++) {
    rmqueue[0].remove();
  }
  console.log(formData);
  // post via fetch
  var headers = new Headers();
  delete headers["Content-Type"];
  if (hasdata) {
    fetch("/-/update/" + listname, {
      method: "POST",
      body: formData,
      headers: headers,
    }).then(function (response) {
      console.log(response);
    });
  }
}

function connect(timeout) {
  let prefix = "wss://";
  if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
    prefix = "ws://";
  }
  webSocket = new WebSocket(prefix + window.location.host + websocketpath);
  webSocket.onmessage = function (event) {
    console.log(event.data);
    var data = JSON.parse(event.data);
    if (data.type == "add") {
      console.log("adding item");
      if (!document.getElementById(data.cleanedname)) {
        addItem(data.name, data.cleanedname);
      }
    } else if (data.type == "remove") {
      var elements = document.getElementsByClassName(data["cleanedname"]);
      console.log(elements, elements.length);
      for (var i = 0; i < elements.length; i++) {
        elements[i].parentElement.previousElementSibling.remove();
        elements[i].parentElement.remove();
      }
    } else if (data.type == "cross") {
      var label = document.getElementsByClassName(
        data["cleanedname"] + "label"
      );
      var label = label[0];
      checkbox = label.previousSibling.previousSibling;
      console.log(checkbox);
      console.log(checkbox.checked);
      if (!checkbox.checked) {
        label.className = label.className + " crossindicator";
      }
    } else if (data.type == "uncross") {
      var label = document.getElementsByClassName(
        data["cleanedname"] + "label"
      );
      var label = label[0];
      label.className = label.className.replace(" crossindicator", "");
    } else if (data.type == "onlinecount") {
      document.getElementById("onlinecount").innerHTML = data["name"];
    } else if (data.type == "login") {
      window.location.replace("/login");
    }
  };
  webSocket.onopen = function (event) {
    console.log("WebSocket connection established");
    var data = {
      type: "online",
      name: username,
    };
    var statusindicator = document.getElementById("status");
    statusindicator.style.backgroundColor = "green";
    webSocket.send(JSON.stringify(data));
    var form = document.getElementById("list");
    form.querySelectorAll("input[type=submit]")[0].disabled = false;
    sendqueue();
    online = true;
  };
  webSocket.onclose = function (event) {
    console.log("WebSocket closed");
    var statusindicator = document.getElementById("status");
    statusindicator.style.backgroundColor = "red";
    online = false;
    setTimeout(function () {
      connect(timeout * 1.2);
    }, timeout);
  };
}
connect();

var form = document.getElementById("list");
console.log(form);
function formSubmit(event) {
  console.log("submit");
  if (!form) {
    form = document.getElementById("list");
  }
  const formData = new FormData(form);
  console.log(formData.get("add"));
  if (!online) {
    event.preventDefault();
    values = document.getElementById("last");
    if (values.value != "") {
      values = values.value.split(",");
      console.log(values);
      console.log(values.length);
      for (var i = 0; i < values.length; i++) {
        console.log(i);
        v = values[i].trim();
        console.log(v);
        if (v != "") {
          var cleanedname = values[i].replace(/[^a-zA-Z0-9]/g, "");
          element = addItem(v, cleanedname);
          console.log(element);
          if (element) {
            element.className += " addqueue";
          }
        }
      }
      document.getElementById("last").value = "";
    }
    // iterate through all checkboxes in form, and make them red and disabled if checked
    var checkboxes = form.querySelectorAll("input[type=checkbox]");
    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        checkboxes[i].disabled = true;
        // find the label for this checkbox
        var label = document.getElementsByClassName(checkboxes[i].id + "label");
        if (label.length > 0) {
          label = label[0];
          label.className += " rmqueue";
        }
      }
    }
  }
  console.log(event);
}

function syncCrossed(elem) {
  let typ = "";
  if (elem.checked) {
    typ = "cross";
  } else {
    typ = "uncross";
  }
  const data = {
    type: typ,
    name: elem.name,
  };
  console.log(data);
  webSocket.send(JSON.stringify(data));
}

function showFullscreen(event) {
  overlayimg.src = event.target.src;
  overlay.style.display = "block";
}

function hideFullscreen() {
  overlay.style.display = "none";
}

const duration = 150;
function dragenter(event) {
  anime({
    targets: event.target,
    height: "40px",
    easing: "easeInOutSine",
    duration: duration,
  });
  anime({
    targets: "#space",
    height: 5,
    easing: "easeInOutSine",
    duration: duration,
  });
}
function dragleave(event) {
  anime({
    targets: event.target,
    height: "20px",
    easing: "easeInOutSine",
    duration: duration,
  });
  anime({
    targets: "#space",
    height: 25,
    easing: "easeInOutSine",
    duration: duration,
  });
}

function dragstarthandler(e) {
  e.dataTransfer.setData("id", e.target.id);
  e.dataTransfer.dropEffect = "move";

  setTimeout(() => {
    let prev = e.target.parentNode.previousElementSibling;
    prev.id = "draggingelementprev";
    const hide = document.getElementById("hidden");
    hide.insertAdjacentElement("beforeend", e.target.parentNode);

    let space = document.createElement("div");
    space.id = "space";
    space.style.height = e.target.offsetHeight;
    prev.after(space);

    anime({
      targets: space,
      height: 25,
      easing: "easeInOutSine",
      duration: duration,
    });
  }, 1);
}

function onDrag(e) {}

function onendDrag(e) {
  const prev = document.getElementById("draggingelementprev");
  prev.after(e.target.parentNode);
  prev.id = "";
  document.getElementById("space").remove();
  anime.set(e.target, { scaleY: 0.3 });
  anime({
    targets: e.target,
    scaleY: 1,
    easing: "easeInOutSine",
    duration: duration,
  });
}

function onDrop(ev) {
  let after = "";
  if (event.target.previousElementSibling != null) {
    after = event.target.previousElementSibling
      .querySelector("label")
      .childNodes[0].nodeValue.trim();
  }
  anime({
    targets: event.target,
    height: "20px",
    easing: "easeInOutSine",
    duration: duration,
  });
  const itm = document.getElementById(ev.dataTransfer.getData("id"));
  anime.set(itm, { scaleY: 0.3 });
  anime({
    targets: itm,
    scaleY: 1,
    easing: "easeInOutSine",
    duration: duration,
  });
  const item = itm.parentNode;
  const newdroparea = document.getElementById("draggingelementprev");
  ev.target.parentNode.insertBefore(item, ev.target);
  ev.target.parentNode.insertBefore(newdroparea, item);
  let data = {
    type: "move",
    name: itm.childNodes[0].nodeValue.trim(),
    after: after,
  };
  webSocket.send(JSON.stringify(data));
}
