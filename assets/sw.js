var version = "v2";

self.addEventListener("install", (event) => {
  event.waitUntil(
    caches.open(version).then((cache) => {
      return cache.addAll([
        "./lists/",
        "./assets/style.css",
        "./assets/app.js",
        "./assets/list.js",
      ]);
    })
  );
});

self.addEventListener("fetch", (event) => {
  if (event.request.method != "GET") {
    console.log("Not a GET request");
    event.respondWith(
      fetch(event.request).then((resp) => {
        console.log("Not a GET request");
        return resp;
      })
    );
  } else {
    console.log("GET request");
    //     event.respondWith(
    //       fetch(event.request)
    //         .then((response) => {
    // 	  console.log("GET request");
    //           console.log("fetched");
    //           var rsp = response.clone();
    //           console.log(response.status);
    //           if (response.status < 300) {
    //             console.log("successfull fetch");
    //             caches.open(version).then((cache) => {
    //               cache.put(event.request, rsp);
    //             });
    //             return response;
    //           } else if (response.status > 400) {
    //             return response;
    //           } else {
    //             console.log("failed fetch try using cached");
    //             console.log(event.request);
    //             return caches
    //               .match(event.request)
    //               .then((response) => {
    //                 console.log("returning cached");
    //                 if (response) {
    //                   return response;
    //                 }
    //                 console.log("failed to fetch and no cached, answering 404");
    //                 rsp = new Response('<h1>302</h1><a href="/lists">Lists</a>', {
    //                   status: 302,
    //                   statusText: "Found",
    //                   headers: {
    //                     Location: "/lists",
    //                   },
    //                 });
    //                 return rsp;
    //               })
    //               .catch((error) => {
    //                 // generate a 404 page
    //               });
    //           }
    //         })
    //         .catch((error) => {
    //           console.log(error);
    //           return caches
    //             .match(event.request)
    //             .then((response) => {
    //               return response;
    //             })
    //             .catch((err) => {
    //               rsp = new Response("<h1>404</h1>", {
    //                 status: 404,
    //                 statusText: "Not Found",
    //               });
    //               return rsp;
    //             });
    //         })
    //     );
  }
  //   // return rsp;
  //   // console.log("sw: fetch event");
  //   // // if request paths begins with assets, serve from cache
  //   // // else, fetch from only serve cace if offline
  //   // if (event.request.url.startsWith("/assets")) {
  //   //   console.log("sw: serving asset");
  //   //   caches.match(event.request).then((response) => {
  //   //     return (
  //   //       response ||
  //   //       fetch(event.request).then((response) => {
  //   //         cache.put(event.request, response.clone());
  //   //         return response;
  //   //       })
  //   //     );
  //   //   });
  //   // } else {
  //   //   console.log("sw: not asset");
  //   //   fetch(event.request).then(
  //   //     (response) => {
  //   //       if (response.status == 200) {
  //   //         cache.put(event.request, response.clone());
  //   //         return response;
  //   //       } else {
  //   //         return new Response("can't connect to server", {
  //   //           status: response.status,
  //   //         });
  //   //       }
  //   //     },
  //   //     () => {
  //   //       console.log("sw: fetch catch");
  //   //       return caches.match(event.request).catch(() => {
  //   //         return new Response("You are offline", {
  //   //           status: 503,
  //   //           statusText: "Service Unavailable",
  //   //         });
  //   //       });
  //   //     }
  //   //   );
  //   // }
});

// // self.addEventListener("activate", (event) => {
// //   var cacheKeeplist = [version];
// //   event.waitUntil(
// //     caches.keys().then((keyList) => {
// //       return Promise.all(
// //         keyList.map((key) => {
// //           if (cacheKeeplist.indexOf(key) === -1) {
// //             return caches.delete(key);
// //           }
// //         })
// //       );
// //     })
// //   );
// // });
