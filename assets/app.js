if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/sw.js", { scope: "/" })
    .then(function (reg) {
      // Registrierung erfolgreich
      console.log("Registrierung erfolgreich. Scope ist " + reg.scope);
    })
    .catch(function (error) {
      // Registrierung fehlgeschlagen
      console.log("Registrierung fehlgeschlagen mit " + error);
    });
}

Array.from(document.getElementsByClassName("jsonly")).forEach((el, i, a) =>
  el.classList.remove("jsonly")
);
