package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/komkom/toml"
)

const (
	TemplList      = "list.html"
	TemplMailShare = "sharedmail.html"
	TemplMailLogin = "loginmail.html"
	TemplLists     = "lists.html"
	TemplShared    = "shared.html"
	TemplAbout     = "about.html"
	TemplLogin     = "login.html"
)

var (
	tmplfuncmap = template.FuncMap{"clean": Clean, "namify": Namify, "safejs": safeJS, "safecss": safeCSS}
	templates   = template.Must(template.New(TemplList).Funcs(tmplfuncmap).ParseGlob("templates/*"))
)

var mu = sync.Mutex{}

var lastitems = map[string]struct {
	item string
	time time.Time
}{}

func isin(list []string, item string) bool {
	for _, v := range list {
		if v == item {
			return true
		}
	}
	return false
}

func tostrings(items []Item) []string {
	list := []string{}
	for _, item := range items {
		list = append(list, item.Label)
	}
	return list
}

func newList(username, listname string) (string, error) {
	Log.Println("Creating new list", listname)
	newfile := username + "/" + listname
	if _, err := os.Stat(newfile); !errors.Is(err, os.ErrNotExist) {
		count := 1
		plainfile := newfile
		newfile = plainfile + " - " + strconv.Itoa(count)
		for _, err := os.Stat(newfile); !errors.Is(err, os.ErrNotExist); count++ {
			newfile = plainfile + " - " + strconv.Itoa(count)
			_, err = os.Stat(newfile)
			Log.Println(err)
		}
	}
	err := ioutil.WriteFile(newfile, []byte(""), 0600)
	if err != nil {
		return "", err
	}
	return strings.Split(newfile, "/")[1], nil
}

func splitLabel(label string) (string, string) {
	if s := strings.Split(label, " - "); len(s) == 2 {
		return s[0], s[1]
	}
	if s := strings.Split(label, "  "); len(s) == 2 {
		return s[0], s[1]
	}
	pa := regexp.MustCompile(`\(.+\)`)
	if f := pa.FindAllString(label, -1); len(f) > 0 {
		return pa.ReplaceAllString(label, ""), strings.Join(f, " ")
	}
	return label, ""
}

func readList(username string, title string) (*List, error) {
	mu.Lock()
	defer mu.Unlock()
	content, err := ioutil.ReadFile(username + "/" + title)
	if err != nil {
		return nil, err
	}
	var stat syscall.Stat_t
	syscall.Stat(username+"/"+title, &stat)
	inode := strconv.Itoa(int(stat.Ino))
	permf, _ := ioutil.ReadFile(username + "/." + title + ".toml")
	permissions := Permissions{true, true, true, true} // set default permissions
	json.NewDecoder(toml.New(bytes.NewBuffer(permf))).Decode(&permissions)
	labels := strings.Split(string(content), "\n")
	items := []Item{}
	for _, label := range labels {
		imgpath := username + "/.images/" + title + "/" + label + ".png"
		if _, err := os.Stat(imgpath); err == nil {
			img := "/images/" + title + "/" + label + ".png"
			label, extra := splitLabel(label)
			items = append(items, Item{Label: label, Extra: extra, Image: img})
		} else {
			if _, err := os.Stat(username + "/.images"); os.IsNotExist(err) { // I could probably also use os.MkdirAll
				os.Mkdir(username+"/.images", os.ModePerm)
			} else if _, err := os.Stat(username + "/.images/" + title); os.IsNotExist(err) {
				os.Mkdir(username+"/.images/"+title, os.ModePerm)
			}
			if label != "" {
				label, extra := splitLabel(label)
				items = append(items, Item{Label: label, Extra: extra})
			}
		}
	}
	var online int
	if h, ok := wsHubs[int(stat.Ino)]; ok {
		online = len(h.clients)
	}
	return &List{Title: title,
		Items:         items,
		Permissions:   permissions,
		Len:           len(items),
		SharedAmount:  len(shared(username, title)),
		Username:      username,
		WebSocketPath: "/-/websocket/" + inode,
		Inode:         int(stat.Ino),
		Online:        online,
		Color:         getHue(title)}, nil
}

func writeList(username string, list *List) error {
	mu.Lock()
	defer mu.Unlock()
	file := username + "/" + list.Title
	content := ""
	Log.Println(list.Items)
	for _, i := range list.Items {
		if i.Label != "" {
			content = content + i.Label + " - " + i.Extra + "\n"
		}
	}
	f := ioutil.WriteFile(file, []byte(content), 0600)
	return f
}

var strip = regexp.MustCompile(`(?:^ +| +$)`)

// TODO: this gets called twice somehow
func add(username string, listname string, additems []string, img multipart.File) error {
	list, err := readList(username, listname)
	if err != nil {
		return err
	}
	Log.Println("Add to List", additems)
	if list.Permissions.Add == true {
		wsHub, wsExists := wsHubs[list.Inode]
		for _, v := range additems {
			v = strip.ReplaceAllString(v, "")
			if v != "" && !isin(tostrings(list.Items), v) {
				list.Items = append(list.Items, Item{Label: v})
				//trackItem(v, username)
				if wsExists {
					msg := wsMessage{
						Type:        "add",
						Name:        v,
						CleanedName: Clean(v),
					}
					go func() { wsHub.broadcast <- msg }()
				}
			}
		}
		//list.Items = SortItems(list.Items)
		writeList(username, list)
		if img != nil {
			filepath := username + "/.images/" + listname + "/" + additems[0] + ".png"
			imgf, err := os.Create(filepath) // I shouldn't just assume it's a png (but it works)
			if err != nil {
				return err
			}
			io.Copy(imgf, img)
			for _, useritem := range shared(username, listname) {
				if useritem.Label != username {
					os.Link(filepath, useritem.Label+"/.images/"+useritem.SharedFilename+"/"+additems[0]+".png")
				}
			}
		}
		return nil
	} else {
		return errors.New("You don't have the permission to add items to the list")
	}
}

func remove(username string, listname string, rmitems []string) error {
	list, rerr := readList(username, listname)
	if rerr != nil {
		return rerr
	}
	if list.Permissions.Remove == true {
		nlist := []Item{}
		for _, item := range list.Items {
			ii := isin(rmitems, item.Label)
			if !ii && (item.Label != "") {
				nlist = append(nlist, Item{Label: item.Label, Extra: item.Extra})
			} else if item.Image != "" {
				for _, users := range shared(username, listname) {
					os.Remove(users.Label + "/." + strings.Replace(item.Image[1:], listname, users.SharedFilename, 1))
				}
			}
			if ii {
				msg := wsMessage{
					Type:        "remove",
					Name:        item.Label,
					CleanedName: Clean(item.Label),
				}
				if hub, ok := wsHubs[list.Inode]; ok {
					go func() { hub.broadcast <- msg }()
				}
			}
		}
		list.Items = nlist
		writeList(username, list)
		/*
			go func() {
				if len(rmitems) > 1 {
					for n, neighbora := range rmitems {
						for _, neighborb := range rmitems[n+1:] {
							WriteNeighbors(neighbora, neighborb)
						}
					}
				} else if len(rmitems) == 1 {
					if li, ok := lastitems[username+"/"+listname]; ok {
						fivemin, _ := time.ParseDuration("5m")
						if time.Now().Sub(li.time) < fivemin {
							WriteNeighbors(li.item, rmitems[0])
						}
					}
					lastitems[username+"/"+listname] = struct {
						item string
						time time.Time
					}{
						item: rmitems[0],
						time: time.Now()}
				}
			}()
		*/
		return nil
	} else {
		return errors.New("You don't have the permission to remove items from the list")
	}
}

func move(username, listname, item, after string) error {
	list, err := readList(username, listname)
	if err != nil {
		return err
	}

	var itemindex, afterindex int
	if after == "" {
		afterindex = -1
	}
	for i, v := range list.Items {
		if v.Label == item {
			itemindex = i
		}
		if v.Label == after {
			afterindex = i
		}
	}
	if afterindex < itemindex {
		// [:afterindex+1] + [itemindex] + [afterindex+1:itemindex] + [itemindex+1:]
		newitems := make([]Item, len(list.Items))
		newitems = append(newitems, list.Items[:afterindex+1]...)
		newitems = append(newitems, list.Items[itemindex])
		newitems = append(newitems, list.Items[afterindex+1:itemindex]...)
		if len(list.Items)-1 != itemindex {
			newitems = append(newitems, list.Items[itemindex+1:]...)
		}
		list.Items = newitems
	} else {
		// [:itemindex] + [itemindex+1:afterindex+1] + [itemindex] + [afterindex+1:]
		newitems := make([]Item, len(list.Items))
		newitems = append(newitems, list.Items[:itemindex]...)
		newitems = append(newitems, list.Items[itemindex+1:afterindex+1]...)
		newitems = append(newitems, list.Items[itemindex])
		newitems = append(newitems, list.Items[afterindex+1:]...)
		list.Items = newitems
	}

	return list.save()
}

func shared(username string, listname string) (sharedwith []Item) {
	dir, err := ioutil.ReadDir(".")
	if err != nil {
		return
	}
	l, err := os.Open(username + "/" + listname)
	if err != nil {
		return
	}
	list, err := l.Stat()
	if err != nil {
		return
	}
	for _, file := range dir {
		if file.IsDir() {
			lists, _ := ioutil.ReadDir(file.Name())
			for _, rlist := range lists {
				if os.SameFile(list, rlist) {
					sharedwith = append(sharedwith, Item{Label: file.Name(), SharedFilename: rlist.Name()})
				}
			}
		}
	}
	return
}
