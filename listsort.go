package main

import (
	"net/http"
	"sort"
	"time"
)

// TODO: sort by most frequently used
// TODO: use exponentional smoothing to sort lists

func saveTimestamp(w http.ResponseWriter, r *http.Request, listname string) {
	session, _ := store.Get(r, "lists")
	session.Values[listname] = time.Now().Unix()
	session.Save(r, w)
}

func sortLists(r *http.Request, lists []*List) []*List {
	session, _ := store.Get(r, "lists")
	sort.Slice(lists, func(i, j int) bool {
		iv := session.Values[lists[i].Title]
		jv := session.Values[lists[j].Title]
		if iv == nil || jv == nil {
			return false
		}
		return iv.(int64) > jv.(int64)
	})
	return lists
}
