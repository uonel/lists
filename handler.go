package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"syscall"

	"github.com/komkom/toml"
)

var listcache = make(map[string]*List)

func StartHandler(w http.ResponseWriter, r *http.Request, username string) {
	w.Header().Set("Content-Type", "text/html")
	if r.URL.Path == "/favicon.ico" {
		http.Redirect(w, r, "/icon/32", http.StatusFound)
		return
	}
	if r.URL.Path == "/" {
		cookie, err := r.Cookie("lastlist")
		if err != nil {
			http.Redirect(w, r, "/lists", http.StatusFound)
			return
		}
		http.Redirect(w, r, "/"+cookie.Value, http.StatusFound)
	} else {
		listname := r.URL.Path[1:]
		var list *List
		if l, ok := listcache[username+"/"+listname]; ok {
			list = l
			delete(listcache, username+"/"+listname)
		}
		var err error
		list, err = readList(username, listname)
		if err != nil {
			if strings.Contains(r.Header.Get("Accept"), "application/json") {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusNotFound)
				json.NewEncoder(w).Encode(err)
				return
			}
			addMessage(w, r, "Konnte Liste \""+listname+"\" nicht öffnen")
			if !strings.HasPrefix(r.URL.Path, "/-/") {
				http.Redirect(w, r, "/-"+r.URL.Path, http.StatusTemporaryRedirect)
				return
			}
			http.Redirect(w, r, "/-/lists", http.StatusFound)
			return
		}

		if strings.Contains(r.Header.Get("Accept"), "application/json") {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(struct {
				Title string
				Items []string
			}{
				Title: list.Title,
				Items: tostrings(list.Items)})
			return
		}

		session, _ := store.Get(r, "store")
		session.Values["lastlist"] = listname
		session.Save(r, w)

		if list.Permissions.Read == false {
			list.Items = []Item{}
		}

		saveTimestamp(w, r, listname)

		tdata := getTemplateData(w, r, username)
		tdata.Data = list
		for _, k := range r.URL.Query()["cross"] {
			for i, v := range list.Items {
				if Clean(v.Label) == k {
					list.Items[i].Crossed = true
				}
			}
		}
		err = templates.ExecuteTemplate(w, TemplList, &tdata)
		if err != nil {
			Log.Println(err)
		}
	}
}

func ListsHandler(w http.ResponseWriter, r *http.Request, username string) {
	user, err := ReadUser(username, true)
	if err != nil {
		Log.Println(err)
		addMessage(w, r, "Konnte Benutzer nicht finden:"+err.Error())
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	user.Lists = sortLists(r, user.Lists)
	if strings.Contains(r.Header.Get("Accept"), "application/json") {
		Log.Println("json")
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(struct {
			User  string   `json:"user"`
			Lists []string `json:"lists"`
		}{
			User:  Namify(user.Username),
			Lists: user.listNames()})
		return
	}
	tdata := getTemplateData(w, r, username)
	tdata.Data = user
	eerr := templates.ExecuteTemplate(w, "lists.html", tdata)
	fmt.Println(eerr, nil, eerr == nil, eerr != nil)
	if eerr != nil {
		fmt.Println(eerr != nil)
		Log.Println("Server Fehler", eerr)
		addMessage(w, r, "Server Fehler: "+eerr.Error())
		w.Write([]byte(eerr.Error()))
	}
}

func UpdateHandler(w http.ResponseWriter, r *http.Request, username string) {
	listname := r.URL.Path[len("/-/update/"):]
	if strings.Contains(r.Header.Get("Content-Type"), "application/json") {
		Log.Println("json")
		var data struct {
			Rmitems  []string `json:"remove"`
			Additems []string `json:"add"`
			Wait     bool     `json:"wait"`
		}
		err := json.NewDecoder(r.Body).Decode(&data)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}
		if data.Wait {
			add(username, listname, data.Additems, nil)
			remove(username, listname, data.Rmitems)
		} else {
			go func() {
				Log.Println("update", data.Rmitems, data.Additems)
				add(username, listname, data.Additems, nil)
				remove(username, listname, data.Rmitems)
			}()
		}
		return
	}
	r.ParseForm()
	r.ParseMultipartForm(10 << 20)

	if pusher, ok := w.(http.Pusher); ok {
		Log.Println("pusher")
		err := pusher.Push("/"+listname, nil)
		if err != nil {
			Log.Println(err)
		} else {
			Log.Println("pushed")
		}
	}
	http.Redirect(w, r, "/"+listname, http.StatusFound)

	go func() {
		rmitems := []string{}
		for k, v := range r.Form {
			if k != "submit" && k != "add" && k != "image" {
				rmitems = append(rmitems, v[0])
			}
		}
		additems := strings.Split(r.FormValue("add"), ", ")
		file, _, _ := r.FormFile("image")

		list, err := readList(username, listname)
		if err != nil {
			addMessage(w, r, "Konnte Liste \""+listname+"\" nicht öffnen")
			http.Redirect(w, r, "/lists", http.StatusFound)
			return
		}

		for _, v := range additems {
			if v != "" {
				go sendWsMessage(list.Inode, "add", v)
			}
		}
		for _, v := range rmitems {
			go sendWsMessage(list.Inode, "remove", v)
		}

		list.addStrings(additems)
		list.removeStrings(rmitems)

		if file != nil {
			list.addImage(additems[0], file)
		}

		listcache[username+"/"+listname] = list
		list.save()

		if len(listcache) > 5 {
			listcache = make(map[string]*List)
		}

	}()

}

func NewHandler(w http.ResponseWriter, r *http.Request, username string) {
	if r.Method == "POST" {
		listname := r.FormValue("new")
		listname = strings.Trim(listname, " ")
		if listname == "" {
			http.Redirect(w, r, "/lists", http.StatusFound)
			return
		}
		listname, err := newList(username, listname)
		if err != nil {
			addMessage(w, r, "Liste konnte nicht erstellt werden: "+err.Error())
			http.Redirect(w, r, "/lists", http.StatusFound)
			return
		}
		http.Redirect(w, r, "/"+listname, http.StatusFound)
	}
}

func DeleteHandler(w http.ResponseWriter, r *http.Request, username string) {
	listname := r.FormValue("list")
	err := os.Remove(username + "/" + listname)
	if err != nil {
		addMessage(w, r, "Liste konnte nicht gelöscht werden")
		http.Redirect(w, r, "/lists", http.StatusFound)
		return
	}
	err = os.Remove(username + "/." + listname + ".toml")
	if err != nil {
		fmt.Println(err)
	}
	err = os.Remove(username + "/.images/" + listname)
	if err != nil {
		fmt.Println(err)
	}
	addMessage(w, r, "Liste wurde gelöscht")
	http.Redirect(w, r, "/lists", http.StatusFound)
}

func ShareHandler(w http.ResponseWriter, r *http.Request, username string) {
	list, lerr := readList(username, r.FormValue("list"))
	if lerr != nil {
		addMessage(w, r, "Konnte Liste nicht öffnen:"+lerr.Error())
		http.Redirect(w, r, "/lists", 302)
		return
	}

	urlf, err := ioutil.ReadFile("aliases.toml")
	if err != nil {
		Log.Println("could not read urls.toml:", err.Error())
	}
	urlm := make(map[string]string)
	json.NewDecoder(toml.New(bytes.NewBuffer(urlf))).Decode(&urlm)

	if list.Permissions.Share {
		query := "?"
		sharedwith := []string{}
	Sharewith:
		for _, sharewith := range Fields(r.FormValue("sharewith")) {
			sharewith = strings.ToLower(sharewith)
			if url, ok := urlm[sharewith]; ok {
				sharewith = url
			}
			if !isEmail(sharewith) {
				addMessage(w, r, "Konnte nicht mit "+sharewith+" teilen, weil es keine E-Mail Adresse ist")
				continue
			}

			query += "n=" + url.QueryEscape(sharewith) + "&"

			originalfile := username + "/" + list.Title
			newfile := sharewith + "/" + list.Title
			// check if sharewith is a user
			if _, err := os.Stat(sharewith); os.IsNotExist(err) {
				os.Mkdir(sharewith, 0755)
			}
			// iterate through all lists of new user, to check if list already exists
			files, err := ioutil.ReadDir(sharewith)
			if err != nil {
				Log.Println(err.Error())
			}
			for _, f := range files {
				st, err := os.Stat(sharewith + "/" + f.Name())
				if err != nil {
					Log.Println(err.Error())
					return
				}
				if st.IsDir() {
					continue
				}
				inode := st.Sys().(*syscall.Stat_t).Ino
				if int(inode) == list.Inode {
					addMessage(w, r, "Liste ist schon mit "+sharewith+" geteilt")
					continue Sharewith
				}

			}

			// numbers are prefixed to listnames to avoid overwriting existing files
			if _, err := os.Stat(newfile); !errors.Is(err, os.ErrNotExist) {
				count := 1
				plainfile := newfile
				newfile = plainfile + " - " + strconv.Itoa(count)
				for st, err := os.Stat(newfile); !errors.Is(err, os.ErrNotExist); count++ {
					// get inode
					inode := st.Sys().(*syscall.Stat_t).Ino
					if list.Inode == int(inode) {
						addMessage(w, r, "Liste ist schon mit "+sharewith+" geteilt")
						continue Sharewith
					}
					newfile = plainfile + " - " + strconv.Itoa(count)
					_, err = os.Stat(newfile)
					log.Println(err)
				}
			}
			lerr := os.Link(originalfile, newfile)
			if lerr != nil {
				Log.Println(lerr)
				addMessage(w, r, "Konnte Liste nicht teilen:"+lerr.Error())
				http.Redirect(w, r, "/shared/"+list.Title, http.StatusFound)
				return
			}
			sharedwith = append(sharedwith, sharewith)
			r.ParseForm()
			permf := ""
			if _, read := r.Form["read"]; read {
				permf = permf + "Read=true\n"
			} else {
				permf = permf + "Read=false\n"
			}
			if _, add := r.Form["add"]; add {
				permf = permf + "Add=true\n"
			} else {
				permf = permf + "Add=false\n"
			}
			if _, remove := r.Form["remove"]; remove {
				permf = permf + "Remove=true\n"
			} else {
				permf = permf + "Remove=false\n"
			}
			if _, share := r.Form["share"]; share {
				permf = permf + "Share=true\n"
			} else {
				permf = permf + "Share=false\n"
			}
			ioutil.WriteFile(sharewith+"/."+list.Title+".toml", []byte(permf), 0644) // I think for more security you should first write the permission file and only set the link if that worked.
			// send email
			newtitle := strings.Split(newfile, "/")[1]
			token, err := generateToken(sharewith)
			if err != nil {
				Log.Println("could not generate token:", err.Error())
			}
			link := "https://" + r.Host + "/" + url.QueryEscape(newtitle) + "?session=" + token
			go send_shared(username, newtitle, sharewith, link)
		}
		if len(sharedwith) == 1 {
			addMessage(w, r, "Liste wurde erfolgreich mit "+sharedwith[0]+" geteilt")
		} else if l := len(sharedwith); l > 1 {
			addMessage(w, r, "Liste wurde erfolgreich mit "+strings.Join(sharedwith[:l-2], ", ")+"und "+sharedwith[l-1]+" geteilt")
		}
		http.Redirect(w, r, "/shared/"+list.Title+query[:len(query)-1], http.StatusFound)
	} else {
		addMessage(w, r, "Du hast nicht die Berechtigung die Liste zu teilen")
		http.Redirect(w, r, "/lists", http.StatusFound)
	}

}

func SharedHandler(w http.ResponseWriter, r *http.Request, username string) {
	listname := r.URL.Path[len("/-/shared/"):]
	sharedwith := shared(username, listname)

	// highlight new added users
	r.ParseForm()
	n := r.Form["n"]
	mp := make(map[string]bool)
	for _, n := range n {
		mp[n] = true
	}

	tdata := getTemplateData(w, r, username)
	tdata.Data = &List{Title: listname, Items: sharedwith}
	tdata.Map = mp
	templates.ExecuteTemplate(w, "shared.html", tdata)
}

func ImagesHandler(w http.ResponseWriter, r *http.Request, username string) {
	img := r.URL.Path[len("/-/images/"):]
	w.Header().Set("Content-Type", "image/png")
	http.ServeFile(w, r, username+"/.images/"+img)
}

func MailHandler(w http.ResponseWriter, r *http.Request, username string) {
	r.ParseForm()
	username = r.FormValue("username")
	send_mail(username, r.FormValue("text"))
	fmt.Fprintf(w, "Mail wurde versendet")
}

func DeleteWebauthnCredHandler(w http.ResponseWriter, r *http.Request, username string) {
	credname := r.FormValue("credname")
	deleteCred(username, credname)
	http.Redirect(w, r, "/", http.StatusFound)
}
