package main

import (
	"crypto/sha1"
	"image"
	"image/color"
	"image/draw"
	_ "image/png"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/disintegration/gift"
	"github.com/disintegration/imaging"
	"github.com/golang/freetype"
	"github.com/pkg/errors"
)

func IconHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Cache-Control", "public, max-age=31536000")
	// handle requests for /icon/<list_name>/<size>
	// both parameters are optional, the list doesn't have to exist
	params := strings.Split(r.URL.Path[len("/-/icon/"):], "/")
	listname, size, err := parseParams(params)
	if err != nil {
		addMessage(w, r, "Error Reading Parameters"+err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Open the image file.
	src, err := imaging.Open("icon.png")
	if err != nil {
		addMessage(w, r, "Error opening image file: "+err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if src.Bounds().Dx() != src.Bounds().Dy() {
		addMessage(w, r, "Icon is not square")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if listname == "" {
		// no listname given, serve src
		if size == 0 {
			err = imaging.Encode(w, src, imaging.PNG)
			if err != nil {
				addMessage(w, r, "Error encoding image: "+err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		// resize src
		dst := imaging.Resize(src, size, size, imaging.Lanczos)
		err = imaging.Encode(w, dst, imaging.PNG)
		if err != nil {
			addMessage(w, r, "Error encoding image: "+err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}

	s := 3 // sigma for blur
	if size != 0 && size > 500 {
		s = 2 // set lower sigma for bigger icons
	}
	blurred := imaging.Blur(src, float64(s))

	hue := getHue(listname) // generate color from listname
	colorized := colorize(blurred, hue)

	// get image of first letter
	letter, err := getFontImage(string(listname[0]))
	if err != nil {
		addMessage(w, r, "Error getting font image: "+err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error getting font image:" + err.Error()))
		return
	}

	// make letter thicker for small icons
	alpha := 0.9
	if size != 0 && size < 20 {
		letter = imaging.Blur(letter, 2)
		letter = imaging.AdjustFunc(letter, func(c color.NRGBA) color.NRGBA {
			if c.A > 50 {
				c.A = 255 // make it opaque
			}
			return c
		})
		alpha = 1 // disable transparency for small icons
	}
	letter = cutToContent(letter) // remove empty space, so it will be centered

	clx := letter.Bounds().Dx()
	cly := letter.Bounds().Dy()
	srcs := src.Bounds().Dx() // source image size
	var lx, ly int
	if clx > cly {
		lx = int(float32(srcs) * 0.8)
		ly = int(clx / cly * lx)
	} else {
		ly = int(float32(srcs) * 0.8)
		lx = int(clx / cly * ly)
	}
	letter = imaging.Resize(letter, lx, ly, imaging.Lanczos)
	combined := imaging.OverlayCenter(colorized, letter, alpha)
	if size == 0 {
		// no size given, use original size
		err = imaging.Encode(w, combined, imaging.PNG)
		if err != nil {
			addMessage(w, r, "Error encoding image: "+err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}
	resized := imaging.Resize(combined, size, size, imaging.Lanczos)
	if size > 500 {
		resized = imaging.Sharpen(resized, 8)
	}
	err = imaging.Encode(w, resized, imaging.PNG)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		addMessage(w, r, "Error encoding image: "+err.Error())
		w.Write([]byte("Error encoding image:" + err.Error()))
		return
	}
}

func parseParams(params []string) (listname string, size int, err error) {
	if len(params) > 1 {
		size, err := strconv.Atoi(params[0])
		if err != nil {
			listname = params[0]
			size, err = strconv.Atoi(params[1])
			if err != nil {
				return listname, size, errors.New("Invalid size parameter")
			}
		} else {
			listname = params[1]
		}
		return listname, size, nil
	} else {
		if n, err := strconv.Atoi(params[0]); err == nil {
			size = n
		} else {
			listname = params[0]
		}
	}
	return listname, size, nil
}

func getHue(listname string) (c int) {
	s := sha1.Sum([]byte(listname))
	for _, b := range s { // []byte(listname) {
		//h = int(b) + (h << 5) - h
		c += int(b)
	}
	return c % 360
}

func getFontImage(letter string) (image.Image, error) {
	fbytes, err := ioutil.ReadFile("font.ttf")
	if err != nil {
		return nil, errors.Wrap(err, "Error reading font file")
	}
	font, err := freetype.ParseFont(fbytes)
	if err != nil {
		return nil, errors.Wrap(err, "Error parsing font file")
	}
	// create a context for drawing text
	fg, bg := image.Black, image.Transparent
	rgba := image.NewRGBA(image.Rect(0, 0, 100, 100))
	draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)
	c := freetype.NewContext()
	c.SetDPI(72)
	c.SetFont(font)
	c.SetFontSize(72)
	c.SetClip(rgba.Bounds())
	c.SetDst(rgba)
	c.SetSrc(fg)
	// draw the text
	pt := freetype.Pt(10, 10+int(c.PointToFixed(72)>>6))
	_, err = c.DrawString(letter, pt)
	if err != nil {
		return nil, errors.Wrap(err, "Error drawing text")
	}
	return rgba, nil
}

func cutToContent(img image.Image) image.Image {
	bounds := img.Bounds()
	minx, miny, maxx, maxy := bounds.Min.X, bounds.Min.Y, bounds.Max.X, bounds.Max.Y
	for x := bounds.Min.X; x < bounds.Max.X; x++ {
		for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
			r, g, b, a := img.At(x, y).RGBA()
			if (x > minx || y > miny) &&
				(r != 0 || g != 0 || b != 0 || a != 0) {
				if x > minx {
					minx = x
				}
				if y > miny {
					miny = y
				}
			}
		}
	}
	// the other way around
	for x := bounds.Max.X; x > bounds.Min.X; x-- {
		for y := bounds.Max.Y; y > bounds.Min.Y; y-- {
			r, g, b, a := img.At(x, y).RGBA()
			if (x < maxx || y < maxy) &&
				(r != 0 || g != 0 || b != 0 || a != 0) {
				if x < maxx {
					maxx = x
				}
				if y < maxy {
					maxy = y
				}

			}
		}
	}
	return imaging.Crop(img, image.Rect(minx, miny, maxx, maxy))
}

func colorize(img image.Image, hue int) image.Image {
	g := gift.New(
		gift.Colorize(float32(hue), 70, 70),
	)
	colorized := image.NewRGBA(g.Bounds(img.Bounds()))
	g.Draw(colorized, img)
	return colorized
}
