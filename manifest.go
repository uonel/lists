package main

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"
)

type WebManifest struct {
	Dir             string `json:"dir,omitempty"`
	Lang            string `json:"lang,omitempty"`
	Name            string `json:"name,omitempty"`
	ShortName       string `json:"short_name,omitempty"`
	Scope           string `json:"scope,omitempty"`
	Description     string `json:"description,omitempty"`
	StartUrl        string `json:"start_url,omitempty"`
	Display         string `json:"display,omitempty"`
	Orientation     string `json:"orientation,omitempty"`
	BackgroundColor string `json:"background_color,omitempty"`
	Shortcuts       []struct {
		Name        string `json:"name,omitempty"`
		ShortName   string `json:"short_name,omitempty"`
		Description string `json:"description,omitempty"`
		Url         string `json:"url,omitempty"`
		Icons       []struct {
			Src   string `json:"src,omitempty"`
			Type  string `json:"type,omitempty"`
			Sizes string `json:"sizes,omitempty"`
		} `json:"icons,omitempty"`
	} `json:"shortcuts,omitempty"`
	Id         string `json:"id,omitempty"`
	ThemeColor string `json:"theme_color,omitempty"`
	Icons      []struct {
		Src   string `json:"src,omitempty"`
		Type  string `json:"type,omitempty"`
		Sizes string `json:"sizes,omitempty"`
	} `json:"icons,omitempty"`
}

func NewManifest() *WebManifest {
	return &WebManifest{
		Dir:         "ltr",
		Lang:        "de",
		Name:        "The Lists App",
		ShortName:   "Lists",
		Scope:       "/",
		Description: "A shared Lists App",
		StartUrl:    "/lists",
		Display:     "standalone",
		Orientation: "any",
		//BackgroundColor: "#f2f2f2", // I don't use these, because of the dark theme
		//ThemeColor:      "#d9d9d9",
		Icons: []struct {
			Src   string `json:"src,omitempty"`
			Type  string `json:"type,omitempty"`
			Sizes string `json:"sizes,omitempty"`
		}{
			{
				Src:   "/icon/192",
				Type:  "image/png",
				Sizes: "192x192",
			},
			{
				Src:   "/icon/512",
				Type:  "image/png",
				Sizes: "512x512",
			},
		},
	}
}

func ManifestHandler(w http.ResponseWriter, r *http.Request) {
	manifest := NewManifest()
	if strings.HasSuffix(r.URL.Path, ".webmanifest") {
		r.URL.Path = r.URL.Path[:len(r.URL.Path)-len(".webmanifest")]
	}
	// TODO: Create some shortcuts to lists
	if b := path.Base(r.URL.Path); b != "manifest" {
		manifest.Name = b
		manifest.ShortName = b
		manifest.Description = "The \"" + b + "\" List"
		manifest.StartUrl = "/" + b
		manifest.Scope = "/" + b
		for n := 0; n < len(manifest.Icons); n++ {
			manifest.Icons[n].Src += "/" + b
		}
	}
	//tkn, _ := generateToken(username)
	//manifest.StartUrl += "?session=" + tkn
	w.Header().Set("Content-Type", "application/manifest+json")
	json.NewEncoder(w).Encode(manifest)
}
