package main

import (
	"io"
	"mime/multipart"
	"os"
	"strings"
)

type List struct {
	Title string
	Items []Item
	Permissions
	Len           int
	SharedAmount  int
	Username      string
	WebSocketPath string
	Inode         int
	Online        int
	Color         int // in deg
}

// Item of a List
type Item struct {
	Label          string
	Extra          string
	Image          string
	SharedFilename string // only used for shared()
	Crossed        bool
}

func (l *List) Text() (s string) {
	s = l.Title + "\n"
	for _, i := range l.Items {
		s += " - " + i.Label + "\n"
	}
	return
}

func (u *User) listNames() (s []string) {
	for _, l := range u.Lists {
		s = append(s, l.Title)
	}
	return
}

func (l *List) add(i []Item) {
	if l.Permissions.Add {
		l.Items = append(l.Items, i...)
		l.Len = len(l.Items)
	}
}

func (l *List) addStrings(s []string) {
	if l.Permissions.Add {
		for _, i := range s {
			l.Items = append(l.Items, Item{Label: strings.Trim(i, " ")})
		}
		l.Len = len(l.Items)
	}
}

// only adds Image, Item still has to be added normally
func (l *List) addImage(label string, img multipart.File) error {
	filepath := l.Username + "/.images/" + l.Title + "/" + label + ".png" // I shouldn't just assume it's a png (though browsers probably correct for it if it's not)
	imgf, err := os.Create(filepath)
	if err != nil {
		return err
	}
	io.Copy(imgf, img)
	for _, useritem := range shared(l.Username, l.Title) {
		if useritem.Label != l.Username {
			os.Link(filepath,
				useritem.Label+"/.images/"+useritem.SharedFilename+"/"+label+".png")
		}
	}
	return nil
}

func (l *List) removeString(s string) {
	for i, item := range l.Items {
		if item.Label == s {
			l.Items = append(l.Items[:i], l.Items[i+1:]...)
			l.Len = len(l.Items)
			if item.Image != "" {
				for _, users := range shared(l.Username, l.Title) {
					os.Remove(users.Label + "/." + strings.Replace(item.Image[1:],
						l.Title, users.SharedFilename, 1))
				}
			}
		}
	}
}

func (l *List) removeStrings(s []string) {
	if l.Permissions.Remove {
		for _, i := range s {
			l.removeString(i)
		}
	}
}

func (l *List) save() error {
	mu.Lock()
	defer mu.Unlock()
	file := l.Username + "/" + l.Title
	var content string
	for _, i := range l.Items {
		if i.Label != "" {
			content = content + i.Label
			if i.Extra != "" {
				content += " - " + i.Extra
			}
			content += "\n"
		}
	}
	f := os.WriteFile(file, []byte(content), 0600)
	return f
}
