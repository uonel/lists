package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type wsMessage struct {
	Type        string `json:"type"`
	Name        string `json:"name"`
	CleanedName string `json:"cleanedname,omitempty"`
	After       string `json:"after,omitempty"` // used by move
}

func (m *wsMessage) Bytes() []byte {
	b, err := json.Marshal(m)
	if err != nil {
		Log.Println("Error marshalling message:", err)
		return nil
	}
	return b
}

type wsClient struct {
	hub  *wsHub
	conn *websocket.Conn
	send chan []byte
	addr string
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

func (c *wsClient) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
		Log.Println("Closed connection")
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			Log.Println("Error reading message:", err)
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		msg := wsMessage{}
		err = json.Unmarshal(message, &msg)
		if err != nil {
			Log.Printf("error: %v", err)
			continue
		}
		switch msg.Type {
		case "add":
			// add(c.hub.username, c.hub.listname, []string{msg.Name}, nil)
		case "remove":
			// remove(c.hub.username, c.hub.listname, []string{msg.Name})
		case "cross":
			msg.CleanedName = Clean(msg.Name)
			go func() { c.hub.broadcast <- msg }()
		case "uncross":
			msg.CleanedName = Clean(msg.Name)
			go func() { c.hub.broadcast <- msg }()
		case "move":
			if err := move(c.hub.username, c.hub.listname, msg.Name, msg.After); err != nil {
				Log.Println(err)
			}
		}

	}
}

func (c *wsClient) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			c.conn.WriteMessage(websocket.TextMessage, message)
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				Log.Println("Error writing ping:", err)
				return
			}
		}
	}
}

type wsHub struct {
	clients    map[*wsClient]bool
	broadcast  chan wsMessage
	register   chan *wsClient
	unregister chan *wsClient
	username   string
	listname   string
	inode      int
}

func (h *wsHub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			go func() { h.broadcast <- wsMessage{Type: "onlinecount", Name: strconv.Itoa(len(h.clients))} }()
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
			go func() { h.broadcast <- wsMessage{Type: "onlinecount", Name: strconv.Itoa(len(h.clients))} }()
			if len(h.clients) == 0 {
				delete(wsHubs, h.inode)
				return
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				client.send <- message.Bytes()
			}
		}
	}
}

var wsHubs = make(map[int]*wsHub)

func sendWsMessage(inode int, typ string, name string) {
	if wsHub, exists := wsHubs[inode]; exists {
		msg := wsMessage{
			Type:        typ,
			Name:        name,
			CleanedName: Clean(name),
		}
		wsHub.broadcast <- msg
	}
}

// Iterate through all files to find the inode
func FilebyInode(inode int, username string) (string, error) {
	// find files in username directory
	files, err := ioutil.ReadDir(username)
	if err != nil {
		return "", err
	}
	for _, list := range files {
		// find files in directory and check if inode is found
		file, err := os.Stat(username + "/" + list.Name())
		if err != nil {
			return "", err
		}
		if int(file.Sys().(*syscall.Stat_t).Ino) == inode {
			return file.Name(), nil
		}
	}
	return "", nil
}

func sendItemsToAddress(address string, items []string, username string, listname string) {
	// wait 4 seconds
	time.Sleep(1 * time.Second)
	list, err := readList(username, listname)
	if err != nil {
		Log.Println("Error reading list:", err)
		return
	}
	if _, ok := wsHubs[list.Inode]; !ok {
		return
	}
	address = strings.Split(address, ":")[0]
	for client := range wsHubs[list.Inode].clients {
		clientaddr := strings.Split(client.addr, ":")[0]
		if clientaddr == address {
			for _, item := range items {
				if item != "" {
					msg := wsMessage{Type: "add", Name: item, CleanedName: Clean(item)}
					client.send <- msg.Bytes()
				}
			}
		}
	}
}

func WebsocketHandler(w http.ResponseWriter, r *http.Request, username string) {
	conn, uerr := upgrader.Upgrade(w, r, nil)
	if uerr != nil {
		log.Println("Error Upgrading Websocket Connection: ", uerr)
	}

	// read file inode from request path
	inode, serr := strconv.Atoi(r.URL.Path[len("/-/websocket/"):])
	if serr != nil {
		log.Println("Error converting inode to int: ", serr)
		return
	}
	if hub, ok := wsHubs[inode]; ok {
		client := &wsClient{hub: hub, conn: conn, send: make(chan []byte), addr: r.RemoteAddr}
		client.hub.register <- client
		go client.writePump()
		go client.readPump()
	} else {
		listname, err := FilebyInode(inode, username)
		if err != nil {
			log.Println("Error getting filename from inode: ", err)
			return
		}
		hub := &wsHub{
			clients:    make(map[*wsClient]bool),
			broadcast:  make(chan wsMessage),
			register:   make(chan *wsClient),
			unregister: make(chan *wsClient),
			username:   username,
			listname:   listname,
			inode:      inode,
		}
		wsHubs[inode] = hub
		go hub.run()
		client := &wsClient{hub: hub, conn: conn, send: make(chan []byte), addr: r.RemoteAddr}
		go client.writePump()
		go client.readPump()
		client.hub.register <- client
	}
}

func websocketLogIn(w http.ResponseWriter, r *http.Request) {
	conn, uerr := upgrader.Upgrade(w, r, nil)
	if uerr != nil {
		log.Println("Error Upgrading Websocket Connection: ", uerr)
		return
	}
	conn.WriteJSON(wsMessage{Type: "login"})
}
