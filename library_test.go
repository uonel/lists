package main

import "testing"

func TestCleanItem(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "clean item",
			want: "clean item",
		},
		{
			name: "Milch (2 Packungen)",
			want: "milch",
		},
		{
			name: "Milch (2 Packungen) - 1,5 Liter",
			want: "milch",
		},
		{
			name: "3 Brote",
			want: "brote",
		},
		{
			name: "//TEST//",
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := cleanItem(tt.name); got != tt.want {
				t.Errorf("cleanItem() = %v, want %v", got, tt.want)
			}
		})
	}
}
