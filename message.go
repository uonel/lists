package main

import (
	"net/http"
)

func addMessage(w http.ResponseWriter, r *http.Request, msg string) {
	session, _ := store.Get(r, "messages")
	session.AddFlash(msg)
	session.Save(r, w)
}

func getMessages(w http.ResponseWriter, r *http.Request) []string {
	session, _ := store.Get(r, "messages")
	flashes := session.Flashes()
	session.Save(r, w)
	msgs := make([]string, len(flashes))
	for i, f := range flashes {
		msgs[i] = f.(string)
	}
	return msgs
}
