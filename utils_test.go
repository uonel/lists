package main

import "testing"

func Test_Namify(t *testing.T) {
	tests := map[string]string{
		"":                       "",
		"paul@example.com":       "Paul",
		"dave1997@example.org":   "Dave",
		"first.last@example.org": "First Last",
	}
	for input, expected := range tests {
		actual := Namify(input)
		if actual != expected {
			t.Errorf("Namify(%q) == %q, expected %q", input, actual, expected)
		}
	}
}
