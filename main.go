package main

import (
	"crypto/sha256"
	"encoding/gob"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"

	"github.com/duo-labs/webauthn/webauthn"
	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"
	"github.com/tdewolff/minify/v2/html"
	"github.com/tdewolff/minify/v2/js"
	"github.com/tdewolff/minify/v2/svg"
)

// User struct
type User struct {
	Username string
	Lists    []*List
}

// implement webauthn.User
func (u User) WebAuthnID() []byte {
	sha := sha256.Sum224([]byte(u.Username))
	return sha[:]
}

func (u User) WebAuthnDisplayName() string {
	return Namify(u.Username)
}

func (u User) WebAuthnName() string {
	return u.Username
}

func (u User) WebAuthnIcon() string {
	return ""
}

func (u User) WebAuthnCredentials() (creds []webauthn.Credential) {
	for _, c := range readCreds(u.Username) {
		creds = append(creds, c)
	}
	return
}

func (u User) WebAuthnCredNames() (creds []string) {
	for n, _ := range readCreds(u.Username) {
		creds = append(creds, n)
	}
	return
}

type TemplateData struct {
	Messages []string
	Username string
	Data     interface{}
	Map      map[string]bool
	Style    string
	Link     string
}

func getTemplateData(w http.ResponseWriter, r *http.Request, username string) TemplateData {
	var data TemplateData
	data.Username = username
	data.Messages = getMessages(w, r)
	return data
}

// Permissions for a list, saved as a .toml file
type Permissions struct {
	Read   bool
	Add    bool
	Remove bool
	Share  bool
}

var Log = log.New(os.Stderr, "", log.Ldate|log.Ltime|log.Lshortfile)

var commit = "unknown"
var host = "example.org"

var min = minify.New()

// middleware, wich adds content-type header based on extension, if not set
func addContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Content-Type") == "" {
			ext := filepath.Ext(r.URL.Path)
			switch ext {
			case ".css":
				w.Header().Set("Content-Type", "text/css")
			case ".js":
				w.Header().Set("Content-Type", "application/javascript")
			case ".svg":
				w.Header().Set("Content-Type", "image/svg+xml")
			case ".html":
				w.Header().Set("Content-Type", "text/html")
			}
		}
		next.ServeHTTP(w, r)
	})
}

// middleware, that adds a 1 year cache header
func cache(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "max-age=31536000")
		next.ServeHTTP(w, r)
	})
}

func serveFile(f string) http.Handler {
	return addContentType(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, f)
	}))
}

func main() {
	Log.Println("Starting Lists...")
	gob.Register(&webauthn.SessionData{})

	min.AddFunc("text/css", css.Minify)
	min.AddFunc("text/html", html.Minify)
	min.AddFunc("image/svg+xml", svg.Minify)
	min.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)

	mux := http.NewServeMux()
	mux.Handle("/-/login/", http.HandlerFunc(LoginHandler))
	mux.HandleFunc("/-/logout", CheckLoggedIn(LogoutHandler))
	mux.Handle("/-/webauthn/login/", http.HandlerFunc(AuthnLogin))
	mux.HandleFunc("/-/webauthn/register", CheckLoggedIn(AuthnRegistration))
	mux.HandleFunc("/-/webauthn/delete", CheckLoggedIn(DeleteWebauthnCredHandler))
	mux.Handle("/-/lists", http.HandlerFunc(CheckLoggedIn(ListsHandler)))
	mux.HandleFunc("/-/new/", CheckLoggedIn(NewHandler))
	mux.HandleFunc("/-/update/", CheckLoggedIn(UpdateHandler))
	mux.HandleFunc("/-/delete/", CheckLoggedIn(DeleteHandler))
	mux.HandleFunc("/-/share/", CheckLoggedIn(ShareHandler))
	mux.Handle("/-/shared/", http.HandlerFunc(CheckLoggedIn(SharedHandler)))
	mux.HandleFunc("/-/images/", CheckLoggedIn(ImagesHandler))
	mux.HandleFunc("/-/newalias/", CheckLoggedIn(NewAliasHandler))
	mux.HandleFunc("/-/websocket/", CheckLoggedIn(WebsocketHandler))
	mux.HandleFunc("/-/mail/", CheckLoggedIn(MailHandler))
	mux.HandleFunc("/-/icon/", IconHandler)
	// mux.HandleFunc("/-/oauth2/auth", authorizeHandlerFunc)
	// mux.HandleFunc("/-/oauth2/token", tokenHandlerFunc)
	mux.Handle("/sw.js", serveFile("assets/sw.js"))
	mux.Handle("/manifest/", http.HandlerFunc(ManifestHandler))
	mux.Handle("/manifest.webmanifest", http.HandlerFunc(ManifestHandler))
	mux.Handle("/", http.HandlerFunc(CheckLoggedIn(StartHandler)))
	fs := http.FileServer(http.Dir("assets/"))
	mux.Handle("/-/assets/", cache(addContentType(http.StripPrefix("/-/assets/", fs))))

	log.Fatal(http.ListenAndServe(":8081", mux))
}
