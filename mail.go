package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/url"
	"strings"
	"time"

	"github.com/emersion/go-message/mail"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

var server string
var mailaddress *mail.Address
var auth sasl.Client = auth_mail()

func auth_mail() sasl.Client {
	// read credentials from file mail.cred
	file, err := ioutil.ReadFile("mail.cred")
	if err != nil {
		Log.Println(err)
		return nil
	}
	// first line is the username, secondline the password,  thirdline the host
	cred := strings.Split(string(file), "\n")
	mailaddress, err = mail.ParseAddress(cred[0])
	if err != nil {
		Log.Println(err)
		return nil
	}
	auth := sasl.NewPlainClient("", cred[0], cred[1])
	server = cred[2] + ":587"
	return auth
}

func send_email(to []*mail.Address, title, text, html string) error {
	var b bytes.Buffer

	var h mail.Header
	h.SetDate(time.Now())
	h.SetAddressList("From", []*mail.Address{mailaddress})
	h.SetAddressList("To", to)
	h.Set("Subject", title)

	mw, err := mail.CreateWriter(&b, h)
	if err != nil {
		return err
	}

	tw, err := mw.CreateInline()
	if err != nil {
		return err
	}
	var th mail.InlineHeader
	th.Set("Content-Type", "text/plain; charset=UTF-8")
	w, err := tw.CreatePart(th)
	if err != nil {
		return err
	}
	io.WriteString(w, text)
	w.Close()

	var hh mail.InlineHeader
	hh.Set("Content-Type", "text/html; charset=UTF-8")
	w, err = tw.CreatePart(hh)
	io.WriteString(w, html)
	w.Close()

	tw.Close()

	tos := make([]string, len(to))
	for i, a := range to {
		tos[i] = a.String()
	}

	mw.Close()
	smtp.SendMail(server, auth, mailaddress.String(), tos, &b)
	return nil
}

func send_mail(from, text string) {
	Log.Println(from, text)
	Log.Println(mailaddress)
	to, err := mail.ParseAddress("_@u1l.de")
	if err != nil {
		Log.Println(err)
		return
	}
	text = from + " hat geschrieben: \r\n" + text
	html := "<html><body><blockquote>" + text + "</blockquote>" + from + "</body></html>"
	err = send_email([]*mail.Address{to}, "Lists Nachricht", text, html)
	if err != nil {
		Log.Println(err)
	}
}

func loginmailtext(code, host, token string, user *User) (s string) {
	s += "Benutz den Code " + code + " um dich anzumelden oder klicke einen Link unten \n\n"
	s += " Hallo " + Namify(user.Username) + "!\n"
	for _, l := range user.Lists {
		s += " - " + l.Title + " [https://" + host + "/" + url.QueryEscape(l.Title) + "?session=" + token + "]\n"
	}
	s += " - Alle Listen [https://" + host + "/lists?session=" + token + "]\n\n"
	return
}

func send_link(to, link, code string) {
	l, err := url.Parse(link)
	if err != nil {
		Log.Println(err)
		return
	}
	toa, err := mail.ParseAddress(to)
	if err != nil {
		Log.Println(err)
		return
	}
	user, err := ReadUser(to, true)
	if err != nil {
		Log.Println(err)
	}

	td := struct {
		Host     string
		Token    string
		Messages []string
		Username string
		Style    string
		Data     *User
	}{
		Host:     l.Host,
		Token:    l.Query().Get("session"),
		Messages: []string{"Benutze den Code " + code + " um dich einzuloggen oder klicke einen Link unten."},
		Username: user.Username,
		Data:     user,
		Style:    stylesheet(),
	}
	text := loginmailtext(code, l.Host, td.Token, user)
	var html bytes.Buffer
	//mw := min.Writer("text/html", &html)
	err = templates.ExecuteTemplate(&html, TemplMailLogin, td)
	if err != nil {
		Log.Println(err)
		return
	}

	err = send_email([]*mail.Address{toa}, "The Lists App Login", text, html.String())
	if err != nil {
		Log.Println(err)
	}
}

func send_shared(from, title, to, link string) {
	toa, err := mail.ParseAddress(to)
	if err != nil {
		Log.Println(err)
		return
	}
	list, err := readList(from, title)
	if err != nil {
		Log.Println(err)
		return
	}
	text := from + " hat die Liste mit dir geteilt \n\n" + list.Text() + "\n\n Öffnen: " + link

	t := TemplateData{
		Messages: []string{from + " hat die Liste mit dir geteilt"},
		Data:     list,
		Style:    stylesheet(),
		Link:     link,
	}
	var html bytes.Buffer
	//mw := min.Writer("text/html", &html)
	//defer mw.Close()
	// TODO: fix
	err = templates.ExecuteTemplate(&html, TemplMailShare, t)
	if err != nil {
		Log.Println(err)
		return
	}
	send_email([]*mail.Address{toa}, title+" | The Lists App", text, html.String())
	if err != nil {
		Log.Println(err)
	}
}
