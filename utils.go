package main

import (
	"bytes"
	"encoding/json"
	"html/template"
	"net/http"
	"os"
	"regexp"
	"strings"
	"unicode"
)

func Fields(s string) []string {
	return strings.FieldsFunc(s, func(r rune) bool { return (r == ',') || unicode.IsSpace(r) })
}

var reg = regexp.MustCompile("[^a-zA-Z0-9]+")
var num = regexp.MustCompile("[0-9]+")

func Clean(str string) string {
	s := reg.ReplaceAllString(str, "")
	s = strings.ToLower(s)
	if len(s) == 0 {
		return "a"
	}
	return s
}

func Namify(o string) string {
	u := strings.Split(o, "@")[0]
	u = reg.ReplaceAllString(u, " ")
	u = num.ReplaceAllString(u, " ")
	u = strings.TrimFunc(u, unicode.IsSpace)
	if u != "" {
		return strings.Title(u) // deprecated, but golang.org/x/text/cases seems hard
	}
	return o
}

func safeJS(s string) template.JS {
	Log.Println("safeHTML", s)
	return template.JS(s)
}

func safeCSS(s string) template.CSS {
	return template.CSS(s)
}

func stylesheet() string {
	stylesheet, err := os.Open("assets/style.css")
	if err != nil {
		Log.Println(err)
		return ""
	}
	var b bytes.Buffer
	if err := min.Minify("text/css", &b, stylesheet); err != nil {
		Log.Println(err)
		return ""
	}
	return b.String()
}

func JSONResponse(w http.ResponseWriter, d interface{}, s int) {
	dj, err := json.Marshal(d)
	if err != nil {
		http.Error(w, "Error creating JSON response", http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(dj)
}

// This doesn't follow the spec. The localpart can contain quotated text, that allows even more characters and
// you can add comments. Also you can use non unicode characters in emails
var emailre = regexp.MustCompile("^(?:[.!#$%&'*+-\\/=?^_\\`{|}~]|[A-Za-z0-9])+@[A-Za-z0-9-]+\\.[A-Za-z]+$")

func isEmail(mail string) bool {
	return emailre.MatchString(mail)
}
